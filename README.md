## Tic Tac Toe

[Demo](Demo.mp4)

### Requirements

- Xcode 9
  * When using the simulator, Xcode 9.1/9.2 betas are recommended: https://www.reddit.com/r/iOSProgramming/comments/75dot0/xcode_91_beta_2_ios_111_beta_2_released/
  * Running on the device is recommended if you want to see with the PBR materials.

### Description

One player and two player Tic Tac Toe. OK AI (still working on perfect play).

### Design decisions

- Chose SceneKit over SpriteKit because being able to use PBR for the board seemed cool.

### TODO

- Add app icon
- Add better PBR materials (from freepbr.com)
- Add more unit tests (complete code coverage), esp. for AI.
- Add UI tests
- Add perfect play AI.
    * Utilize the right heuristic and implement the minimax solution manually (Apple's GameplayKit is a bit opaque / hard to know exactly what's going on underneath).

### Potential future enhancements

- Add different themes
- Add 3D Tic tac Toe, Connect 4, Chess.

### Notes

- PBR assets borrowed from https://github.com/asavihay/PBROrbs-iOS10-SceneKit
- See also: https://developer.apple.com/videos/play/wwdc2016/609/
