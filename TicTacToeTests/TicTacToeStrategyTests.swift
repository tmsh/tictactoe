//
//  TicTacToeStrategyTests.swift
//  TicTacToeTests
//
//  Created by Harada, Thomas on 11/30/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import XCTest
@testable import TicTacToe

class TicTacToeStrategyTests: XCTestCase {
    var gameModel: GameModel!
    override func setUp() {
        super.setUp()

        gameModel = GameModel(gameType: .humanVersusAI, playerOneName: "Tom", playerTwoName: nil, aiStrength: .hard)
    }

    private func assertNotDone() {
        XCTAssertFalse(gameModel.hasWinner)
        XCTAssertFalse(gameModel.isDraw)
    }

    private func assertAIWins() {
        XCTAssertTrue(gameModel.grid.winner == .O)
    }

    private func assertTestWins() {
        XCTAssertTrue(gameModel.grid.winner == .X)
    }

    private func assertItsADraw() {
        XCTAssertTrue(gameModel.isDraw)
    }

    private func testPlays(position: GridPosition) {
        gameModel.updateActivePlayer(position: position)
        gameModel.rotatePlayer()
    }

    private func aiPlays() {
        gameModel.updateActivePlayer(position: gameModel.getBestPositionForActivePlayer()!)
        gameModel.rotatePlayer()
    }

    func testTicTacToeIncompleteCornerStrategy() {
        testPlays(position: GridPosition(row: 0, col: 0))
        aiPlays()
        print(gameModel.grid.description)
        testPlays(position: GridPosition(row: 0, col: 1))
        aiPlays()
        print(gameModel.grid.description)
        testPlays(position: GridPosition(row: -1, col: 1))
        aiPlays()
        print(gameModel.grid.description)
        assertAIWins()
    }
}
