//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by Harada, Thomas on 11/21/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import XCTest
@testable import TicTacToe

class TicTacToeTests: XCTestCase {
    func testTicTacToeWinner() {
        var grid = Grid()
        XCTAssertNil(grid.winner)

        // column tests
        grid.cellValues[0][0] = .X
        grid.cellValues[0][1] = .X
        grid.cellValues[0][2] = .X
        XCTAssertEqual(grid.winner, .X)

        grid = Grid()
        grid.cellValues[1][0] = .X
        grid.cellValues[1][1] = .X
        grid.cellValues[1][2] = .X
        XCTAssertEqual(grid.winner, .X)

        grid = Grid()
        grid.cellValues[2][0] = .X
        grid.cellValues[2][1] = .X
        grid.cellValues[2][2] = .X
        XCTAssertEqual(grid.winner, .X)

        // row tests
        grid = Grid()
        grid.cellValues[0][0] = .X
        grid.cellValues[1][0] = .X
        grid.cellValues[2][0] = .X
        XCTAssertEqual(grid.winner, .X)

        grid = Grid()
        grid.cellValues[0][1] = .X
        grid.cellValues[1][1] = .X
        grid.cellValues[2][1] = .X
        XCTAssertEqual(grid.winner, .X)

        grid = Grid()
        grid.cellValues[0][2] = .X
        grid.cellValues[1][2] = .X
        grid.cellValues[2][2] = .X
        XCTAssertEqual(grid.winner, .X)

        // diagonal tests
        grid = Grid()
        grid.cellValues[0][0] = .X
        grid.cellValues[1][1] = .X
        grid.cellValues[2][2] = .X
        XCTAssertEqual(grid.winner, .X)

        grid = Grid()
        grid.cellValues[0][2] = .X
        grid.cellValues[1][1] = .X
        grid.cellValues[2][0] = .X
        XCTAssertEqual(grid.winner, .X)
    }

    func testTicTacToeDraw() {
        var grid = Grid()
        grid.cellValues[0] = [.X, .O, .X]
        grid.cellValues[1] = [.O, .X, .O]
        grid.cellValues[2] = [.O, .X, .O]
        XCTAssertTrue(grid.isDraw)

        grid = Grid()
        grid.cellValues[0] = [.X, .O, .O]
        grid.cellValues[1] = [.O, .X, .O]
        grid.cellValues[2] = [nil, .X, .X]
        XCTAssertFalse(grid.isDraw)
    }
}
