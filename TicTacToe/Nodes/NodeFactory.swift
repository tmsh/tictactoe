//
//  NodeFactory.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/22/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//
import SceneKit

class NodeFactory {
    /// Create the base grid
    ///
    /// - Returns: an SCNNode
    static func createGrid() -> GridNode {
        return GridNode.create()
    }

    /// Create X node
    ///
    /// - Parameters:
    ///   - position: (-1, -1) to (1, 1)
    ///   - color: color of O
    /// - Returns: an SCNNode
    private static func createX(position: GridPosition, color: UIColor) -> SCNNode {
        // base of X
        let baseNode = SCNNode()
        baseNode.position = SCNVector3(x: Float(CGFloat(position.col) * (NodeConstants.gridWidth/3)), y: Float(CGFloat(position.row) * (NodeConstants.gridWidth/3)), z: 1)

        // \ of X
        let cylinder = SCNCylinder(radius: 0.1, height: NodeConstants.gridWidth/3)
        cylinder.radialSegmentCount = 10
        cylinder.heightSegmentCount = 10
        let cross0 = SCNNode(geometry: cylinder)
        cross0.rotation = SCNVector4(0, 0, 1, NodeConstants.rotPositive45Degrees)
        cross0.position = SCNVector3(x: 0, y: 0, z: 0)
        NodeFactory.addPBRMaterial(to: cross0, materialFilePrefix: "oakfloor-2")
        baseNode.addChildNode(cross0)

        // / of X
        let cross1 = SCNNode(geometry: cylinder)
        cross1.rotation = SCNVector4(0, 0, 1, NodeConstants.rotNegative45Degrees)
        cross1.position = SCNVector3(x: 0, y: 0, z: 0)
        NodeFactory.addPBRMaterial(to: cross1, materialFilePrefix: "oakfloor-2")
        baseNode.addChildNode(cross1)

        return baseNode
    }

    /// Create O node
    ///
    /// - Parameters:
    ///   - position: (-1, -1) to (1, 1)
    ///   - color: color of O
    /// - Returns: an SCNNode
    private static func createO(position: GridPosition, color: UIColor) -> SCNNode {
        // base of O
        let torusGeometry = SCNTorus(ringRadius: (NodeConstants.gridWidth/3)/2 * 0.75, pipeRadius: 0.1)

        let node = SCNNode(geometry: torusGeometry)
        NodeFactory.addPBRMaterial(to: node, materialFilePrefix: "scuffed-plastic")

        node.position = SCNVector3(x: Float(CGFloat(position.col) * (NodeConstants.gridWidth/3)), y: Float(CGFloat(position.row) * (NodeConstants.gridWidth/3)), z: 1)
        node.rotation = SCNVector4(1, 0, 0, NodeConstants.rot90Degrees)
        return node
    }

    /// Create an O or X node
    ///
    /// - Parameters:
    ///   - shape: type of shape to create
    ///   - position: (-1, -1) to (1, 1)
    ///   - color: color of shape
    /// - Returns: an SCNNode
    static func createShape(shape: PlayerShape, position: GridPosition, color: UIColor) -> SCNNode {
        switch shape {
        case .O: return NodeFactory.createO(position: position, color: color)
        case .X: return NodeFactory.createX(position: position, color: color)
        }
    }

    /// Create the camera
    ///
    /// - Returns: an SCNNode
    static func createCamera(screenSize: CGSize) -> SCNNode {
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: NodeConstants.cameraOffset)
        return cameraNode
    }


    /// Add a PBR Material to an SCNNode
    ///
    /// - Parameters:
    ///   - node: the node to add the material to
    ///   - materialFilePrefix: string prefix of PBR image files
    static func addPBRMaterial(to node: SCNNode, materialFilePrefix: String) {
        let material = node.geometry?.firstMaterial

        // Declare that you intend to work in PBR shading mode
        // Note that this requires iOS 10 and up
        material?.lightingModel = SCNMaterial.LightingModel.physicallyBased

        material?.diffuse.contents = UIImage(named: "\(materialFilePrefix)-albedo.png")
        material?.roughness.contents = UIImage(named: "\(materialFilePrefix)-roughness.png")
        material?.metalness.contents = UIImage(named: "\(materialFilePrefix)-metal.png")
        material?.normal.contents = UIImage(named: "\(materialFilePrefix)-normal.png")
    }
}
