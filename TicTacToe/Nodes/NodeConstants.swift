//
//  NodeConstants.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/22/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import SceneKit

class NodeConstants {
    static let rot90Degrees = Measurement(value: 90, unit: UnitAngle.degrees).converted(to: .radians).value
    static let rotPositive45Degrees = Measurement(value: 45, unit: UnitAngle.degrees).converted(to: .radians).value
    static let rotNegative45Degrees = Measurement(value: -45, unit: UnitAngle.degrees).converted(to: .radians).value

    static let cameraOffset: Float = 15
    static let gridWidth: CGFloat = 8
}
