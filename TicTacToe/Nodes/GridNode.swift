//
//  GridNode.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/22/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import SceneKit

class GridNode: SCNNode {
    static func create() -> GridNode {
        let baseGeometry = SCNBox(width: NodeConstants.gridWidth, height: NodeConstants.gridWidth, length: 1.0, chamferRadius: 0.25)
        baseGeometry.widthSegmentCount = 10
        baseGeometry.heightSegmentCount = 10
        baseGeometry.lengthSegmentCount = 10
        baseGeometry.chamferSegmentCount = 10

        let gridNode = GridNode()
        gridNode.geometry = baseGeometry
        gridNode.position = SCNVector3(x: 0, y: 0, z: 0)
        NodeFactory.addPBRMaterial(to: gridNode, materialFilePrefix: "bamboo-wood-semigloss")

        let cylinder = SCNCylinder(radius: 0.1, height: NodeConstants.gridWidth)
        cylinder.radialSegmentCount = 10
        cylinder.heightSegmentCount = 10

        // vertical dividers
        for i in 1...2 {
            let verticalDivider = SCNNode(geometry: cylinder)
            verticalDivider.position = SCNVector3(x: -Float(NodeConstants.gridWidth)/2 + Float(i) * Float(NodeConstants.gridWidth) / 3, y: 0, z: 1)

            NodeFactory.addPBRMaterial(to: verticalDivider, materialFilePrefix: "rustediron-streaks")
            gridNode.addChildNode(verticalDivider)
        }

        // horizontal dividers
        for i in 1...2 {
            let horizontalDivider = SCNNode(geometry: cylinder)
            horizontalDivider.rotation = SCNVector4(0, 0, 1, NodeConstants.rot90Degrees)
            horizontalDivider.position = SCNVector3(x: 0, y: -Float(NodeConstants.gridWidth)/2 + Float(i) * Float(NodeConstants.gridWidth) / 3, z: 1)

            NodeFactory.addPBRMaterial(to: horizontalDivider, materialFilePrefix: "rustediron-streaks")
            gridNode.addChildNode(horizontalDivider)
        }

        // touch pads
        enumerateGrid { gridPosition in
            let box = SCNBox(width: NodeConstants.gridWidth/3 * 0.85, height: NodeConstants.gridWidth/3 * 0.85, length: 1.0, chamferRadius: 0)
            box.materials.first?.diffuse.contents = UIColor.clear
            let pad = SCNNode(geometry: box)
            pad.position = SCNVector3(x: Float(gridPosition.col) * Float(NodeConstants.gridWidth) / 3, y: Float(gridPosition.row) * Float(NodeConstants.gridWidth) / 3, z: 1)
            pad.name = "\(gridPosition.row),\(gridPosition.col)"
            gridNode.addChildNode(pad)
        }
        return gridNode
    }


    func spinBoard() {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 1.9
        rotation = SCNVector4(x: 1, y: 1, z: 1, w: Float(2 * CGFloat(Float.pi)))
        SCNTransaction.commit()
    }
}
