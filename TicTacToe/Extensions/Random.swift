//
//  RandomNumbers.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/22/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

// MARK: - Some extensions for generating random Floats, Ints, Bools and Colors

public extension Float {
    public static func random(min: Float, max: Float) -> Float {
        let r2 = Float(arc4random(UInt32.self)) / Float(UInt32.max)
        return (r2 * (max - min)) + min
    }
}

public func arc4random <T: ExpressibleByIntegerLiteral> (_ type: T.Type) -> T {
    var r: T = 0
    arc4random_buf(&r, Int(MemoryLayout<T>.size))
    return r
}

public extension Int {
    public static func random(min: Int, max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(max - min + 1))) + min
    }
}

public extension Bool {
    public static func random() -> Bool {
        return Int(arc4random_uniform(UInt32(2))) == 1
    }
}

let UIColorOptions: [UIColor] = [
    UIColor.black, // pure black
    UIColor.brown, // peru brown
    UIColor.blue, //blue
    UIColor.cyan, // dark cyan
    UIColor.white, // whitesmoke
    UIColor.red, // crimson
    UIColor.yellow, // light yellow
    UIColor.green, // lawn green
    UIColor.orange, // dark orange
    UIColor.purple // more of a dark magenta
]

public extension UIColor {
    public static func random() -> UIColor {
        let maxValue = UIColorOptions.count
        let rand = Int(arc4random_uniform(UInt32(maxValue)))
        return UIColorOptions[rand]
    }
}
