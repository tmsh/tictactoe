//
//  GameModelUpdate.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/26/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import GameKit

class GameModelUpdate: NSObject {
    var gridPosition: GridPosition
    var updateValue: Int

    required init(gridPosition: GridPosition) {
        self.gridPosition = gridPosition
        self.updateValue = 0
        super.init()
    }
}

extension GameModelUpdate: GKGameModelUpdate {
    var value: Int {
        get {
            return updateValue
        }
        set {
            updateValue = value
        }
    }
}

extension GameModelUpdate: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = GameModelUpdate(gridPosition: self.gridPosition)
        copy.updateValue = self.updateValue
        return copy
    }
}
