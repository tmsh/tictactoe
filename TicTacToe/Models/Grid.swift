//
//  Grid.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/21/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import GameKit

typealias RowArray = Array<PlayerShape?>
typealias GridArray = Array<RowArray>

/// Location within the Tic Tac Toe Grid
/// First value is the column, second value is the row.
/// Values range from (-1, -1) to (1, 1).
struct GridPosition {
    var row: Int
    var col: Int

    var toArrayIndex: GridPosition {
        // For a larger dimension grid (e.g., 4x4), this calculation would have to change.
        return GridPosition(row: row + 1, col: col + 1)
    }
}

func enumerateGrid(evaluate: (GridPosition) -> Void) {
    // For a larger dimension grid (e.g., 4x4), enhance this range.
    for i in -1...1 {
        for j in -1...1 {
            evaluate(GridPosition(row: i, col: j))
        }
    }
}

/// Indicates whether these three cells form a winning solution.
///
/// - Parameters:
///   - first: first cell
///   - second: second cell
///   - third: third cell
/// - Returns: the winner or nil if no winner yet
func areCellsWinner(first: PlayerShape?, second: PlayerShape?, third: PlayerShape?) -> PlayerShape? {
    return first == second && second == third ? first : nil
}

/// The Board model for the X's and O's
class Grid {
    var cellValues = GridArray(repeating: RowArray(repeating: nil, count: 3), count: 3)

    /// Gets the current shape at the grid position
    ///
    /// - Parameter position: (-1, -1) to (1, 1)
    /// - Returns: the grid position
    func getShape(position: GridPosition) -> PlayerShape? {
        let arrayIndex = position.toArrayIndex
        return cellValues[arrayIndex.row][arrayIndex.col]
    }

    /// Sets the current shape at the grid position
    ///
    /// - Parameters:
    ///   - position: (-1, -1) to (1, 1)
    ///   - shape: the shape to update
    func updateShape(position: GridPosition, shape: PlayerShape?) {
        let arrayIndex = position.toArrayIndex
        cellValues[arrayIndex.row][arrayIndex.col] = shape
    }

    /// Returns whether the position is empty and available for playing.
    ///
    /// - Parameters:
    ///   - position: (-1, -1) to (1, 1)
    ///   - shape: the shape to check
    /// - Returns: true or false
    func canPlayPosition(position: GridPosition, shape: PlayerShape) -> Bool {
        return getShape(position: position) == nil
    }

    var freePositions: [GKGameModelUpdate]? {
        var freePositions = [GKGameModelUpdate]()
        enumerateGrid { gridPosition in
            if getShape(position: gridPosition) == nil {
                let gameModelUpdate = GameModelUpdate(gridPosition: gridPosition)
                freePositions.append(gameModelUpdate)
            }
        }
        return freePositions.count == 0 ? nil : freePositions
    }

    /// Returns possible winning positions exist for a shape
    ///
    /// - Parameter shape: the shape that might win
    /// - Returns: positions where the shape would win
    func winningPositions(for shape: PlayerShape) -> [GridPosition] {
        var winningPositions = [GridPosition]()
        enumerateGrid { gridPosition in
            if getShape(position: gridPosition) == nil {
                updateShape(position: gridPosition, shape: shape)
                let foundWinner = winner != nil
                updateShape(position: gridPosition, shape: nil)
                if foundWinner {
                    winningPositions.append(gridPosition)
                }
            }
        }
        return winningPositions
    }

    /// Returns whether the board is at a draw (full but no winner)
    var isDraw: Bool {
        let containsNilValues = cellValues.contains(where: { row in row.contains(where: { cell in cell == nil}) })
        return !containsNilValues && winner == nil
    }

    /// Returns whether the board has a winner
    var winner: PlayerShape? {
        return rowWinner ?? columnWinner ?? diagonalWinner
    }

    /// Indicates whether there is a row winner
    ///
    /// - Returns: the winner or nil if no winner yet
    var rowWinner: PlayerShape? {
        for row in cellValues {
            if let winner = areCellsWinner(first: row[0], second: row[1], third: row[2]) {
                return winner
            }
        }
        return nil
    }

    /// Indicates whether there is a column winner
    ///
    /// - Returns: the winner or nil if no winner yet
    var columnWinner: PlayerShape? {
        for i in 0...2 {
            if let winner = areCellsWinner(first: cellValues[0][i], second: cellValues[1][i], third: cellValues[2][i]) {
                return winner
            }
        }
        return nil
    }

    /// Indicates whether there is a diagonal winner
    ///
    /// - Returns: the winner or nil if no winner yet
    var diagonalWinner: PlayerShape? {
        return areCellsWinner(first: cellValues[0][0], second: cellValues[1][1], third: cellValues[2][2]) ??
            areCellsWinner(first: cellValues[0][2], second: cellValues[1][1], third: cellValues[2][0])
    }

    /// String description of the grid, from top row visually to bottom (so reverse row index sorted).
    var description: String {
        return "--------------------\n\(cellValues[2][0]?.rawValue ?? "_") \(cellValues[2][1]?.rawValue ?? "_") \(cellValues[2][2]?.rawValue ?? "_")\n\(cellValues[1][0]?.rawValue ?? "_") \(cellValues[1][1]?.rawValue ?? "_") \(cellValues[1][2]?.rawValue ?? "_")\n\(cellValues[0][0]?.rawValue ?? "_") \(cellValues[0][1]?.rawValue ?? "_") \(cellValues[0][2]?.rawValue ?? "_")"
    }
}

extension Grid: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Grid()
        copy.cellValues = self.cellValues
        return copy
    }
}
