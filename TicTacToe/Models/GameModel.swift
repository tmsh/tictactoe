//
//  GameModel.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/21/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import GameKit

/// The game model that takes into account player state and board state.
class GameModel: NSObject {
    var gameType: GameType
    var grid = Grid()
    var activePlayerIndex = 0
    var playerModels: [PlayerModel]
    var aiStrength: AIStrength?
    var turnCount = 0
    var debugScores = false

    required init(gameType: GameType, playerOneName: String?, playerTwoName: String?, aiStrength: AIStrength?) {
        self.gameType = gameType
        self.aiStrength = aiStrength
        switch gameType {
        case .humanVersusAI:
            playerModels = [
                PlayerModel(playerType: .playerOne, shape: .X, playerName: playerOneName),
                PlayerModel(playerType: .ai, shape: .O, playerName: nil)
            ]
        case .humanVersusHuman:
            playerModels = [
                PlayerModel(playerType: .playerOne, shape: .X, playerName: playerOneName),
                PlayerModel(playerType: .playerTwo, shape: .O, playerName: playerTwoName)
            ]
        }
        super.init()
    }
}

extension GameModel: GKGameModel {
    var players: [GKGameModelPlayer]? {
        return playerModels
    }

    var activePlayer: GKGameModelPlayer? {
        return playerModels[activePlayerIndex]
    }

    func setGameModel(_ gameModel: GKGameModel) {
        guard let gameModel = gameModel as? GameModel else {
            return
        }
        self.grid = gameModel.grid.copy() as! Grid
        self.activePlayerIndex = gameModel.activePlayerIndex
        self.playerModels = gameModel.playerModels
        self.aiStrength = gameModel.aiStrength
        self.turnCount = gameModel.turnCount
    }

    func gameModelUpdates(for player: GKGameModelPlayer) -> [GKGameModelUpdate]? {
        if hasWinner || isDraw {
            return nil
        }
        return grid.freePositions
    }

    func apply(_ gameModelUpdate: GKGameModelUpdate) {
        guard let gameModelUpdate = gameModelUpdate as? GameModelUpdate else {
            return
        }
        updateActivePlayer(position: gameModelUpdate.gridPosition)
        rotatePlayer()
    }

    func updateActivePlayer(position: GridPosition) {
        guard let activePlayer = activePlayer as? PlayerModel else {
            return
        }
        grid.updateShape(position: position, shape: activePlayer.shape)
    }

    func rotatePlayer() {
        activePlayerIndex = activePlayerIndex == 0 ? 1 : 0
        turnCount += 1
    }

    func score(for player: GKGameModelPlayer) -> Int {
        if let player = player as? PlayerModel {
            if player.playerType == .playerOne {
                print("what")
            }
        }
        let winner = grid.winner
        var score = 0

        if winner != nil {
            score = winner == .O ? 100 : -100
            if aiStrength == .medium {
                if Int.random(min: 0, max: 5) == 0 {
                    return 0
                }
            }
        }
        score -= turnCount
        if debugScores {
            print("score player: \(score), turn count: \(turnCount):")
            print("board: \(grid.description)")
        }
        return score
    }
}

extension GameModel {
    var activePlayerShape: PlayerShape {
        return playerModels[activePlayerIndex].shape
    }
    /// Returns whether the board is playable at this position
    ///
    /// - Parameters:
    ///   - position: position to query
    ///   - shape: shape to query
    /// - Returns: true or false
    func canPlay(position: GridPosition) -> Bool {
        guard let activePlayer = activePlayer as? PlayerModel else {
            return false
        }
        return grid.canPlayPosition(position: position, shape: activePlayer.shape)
    }

    /// Calculate the best position for the active player
    ///
    /// - Returns: a grid position
    func getBestPositionForActivePlayer() -> GridPosition? {
        let strategist = GKMinmaxStrategist()
        guard let gameModel = self.copy() as? GameModel else {
            return nil
        }
        strategist.gameModel = gameModel
        switch aiStrength {
        case .easy?:
            strategist.maxLookAheadDepth = 1
        case .medium?:
            strategist.maxLookAheadDepth = 2
        case .hard?:
            strategist.maxLookAheadDepth = 9
        case .none:
            break
        }
        guard let bestMove = strategist.bestMoveForActivePlayer() as? GameModelUpdate else {
            return nil
        }
        return bestMove.gridPosition
    }

    /// Returns whether the board has a winner
    var hasWinner: Bool {
        return grid.winner != nil
    }

    /// Returns whether board has a draw or not
    var isDraw: Bool {
        return grid.isDraw
    }

    /// Returns the text for the current turn to display to the end user.
    var currentTurnText: String {
        guard let activePlayer = activePlayer as? PlayerModel else {
            return ""
        }
        return activePlayer.currentTurnText
    }

    /// Returns the winner text for display to the end user.
    var winnerText: String {
        guard let activePlayer = activePlayer as? PlayerModel else {
            return ""
        }
        return activePlayer.winnerText
    }
}

extension GameModel: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = GameModel(gameType: self.gameType, playerOneName: nil, playerTwoName: nil, aiStrength: self.aiStrength)
        copy.grid = self.grid.copy() as! Grid
        copy.activePlayerIndex = self.activePlayerIndex
        copy.playerModels = self.playerModels
        copy.aiStrength = self.aiStrength
        copy.turnCount = self.turnCount
        return copy
    }
}
