//
//  PlayerModel.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/26/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import GameKit

enum PlayerType: Int {
    case playerOne = 0
    case playerTwo = 1
    case ai = 2
}

enum GameType: String {
    case humanVersusHuman = "humanVersusHuman"
    case humanVersusAI = "humanVersusAI"
}

enum AIStrength: String {
    case easy = "easy"
    case medium = "medium"
    case hard = "hard"
}

enum PlayerShape: String {
    case X = "X"
    case O = "O"

    func opposite() -> PlayerShape {
        return self == .X ? .O : .X
    }
}

class PlayerModel: NSObject {
    var playerType: PlayerType
    var playerName: String?
    var shape: PlayerShape

    var displayName: String {
        switch playerType {
        case .playerOne:
            return playerName ?? "Player 1️⃣"
        case .playerTwo:
            return playerName ?? "Player 2️⃣"
        case .ai:
            return "The 🤖"
        }
    }

    /// Returns the text for the current turn to display to the end user.
    var currentTurnText: String {
        return "Turn: \(displayName), \(shape.rawValue)"
    }

    /// Returns the winner text for display to the end user.
    var winnerText: String {
        var emoji: String
        switch playerType {
        case .playerOne, .playerTwo:
            emoji = "👍"
        case .ai:
            emoji = "👻"
        }
        return "\(displayName), \(shape.rawValue) wins! \(emoji)"
    }

    init(playerType: PlayerType, shape: PlayerShape, playerName: String?) {
        self.playerType = playerType
        self.shape = shape
        self.playerName = playerName
        super.init()
    }
}

extension PlayerModel: GKGameModelPlayer {
    var playerId: Int {
        return playerType.rawValue
    }
}

extension PlayerModel: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = PlayerModel(playerType: self.playerType, shape: self.shape, playerName: self.playerName)
        return copy
    }
}
