//
//  OverlayScene.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/23/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import Foundation
import SceneKit
import SpriteKit

class OverlayScene: SKScene {
    private var headerNode = SKLabelNode(text: nil)
    private var footerNode = SKLabelNode(text: nil)
    private var menuButton = ButtonNode(text: "Menu")
    private var shareResultsButton = ButtonNode(text: "Share Results")

    var headerText: String = "" {
        didSet {
            headerNode.text = headerText
        }
    }

    var footerText: String = "" {
        didSet {
            footerNode.text = footerText
        }
    }

    var shareResultsIsHidden: Bool = true {
        didSet {
            shareResultsButton.isHidden = shareResultsIsHidden
        }
    }

    var restartAction: (() -> ())?

    override init(size: CGSize) {
        super.init(size: size)

        scaleMode = .resizeFill

        headerNode.fontColor = .black
        footerNode.fontColor = .black
        menuButton.label?.fontColor = .black
        shareResultsButton.label?.fontColor = .black

        headerNode.fontName = "Avenir-Medium"
        footerNode.fontName = "Avenir-Medium"
        menuButton.label?.fontName = "Avenir-Medium"
        shareResultsButton.label?.fontName = "Avenir-Medium"

        headerNode.fontSize = 20
        footerNode.fontSize = 20
        menuButton.label?.fontSize = 20
        shareResultsButton.label?.fontSize = 16

        menuButton.label?.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left

        shareResultsButton.isHidden = true

        footerNode.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right

        addChild(headerNode)
        addChild(footerNode)
        addChild(menuButton)
        addChild(shareResultsButton)

        setConstraints()

        menuButton.buttonTapped = {
            self.restartAction?()
        }

        shareResultsButton.buttonTapped = {
            let lp: BranchLinkProperties = BranchLinkProperties()
            lp.addControlParam("$desktop_url", withValue: "http://s3d-tic-tac-toe.com")
            lp.addControlParam("$ios_url", withValue: "http://s3d-tic-tac-toe.com")
            lp.addControlParam("$ipad_url", withValue: "http://s3d-tic-tac-toe.com")
            lp.addControlParam("$android_url", withValue: "http://s3d-tic-tac-toe.com")
            lp.addControlParam("$match_duration", withValue: "2000")

            lp.addControlParam("custom_data", withValue: "yes")
            lp.addControlParam("look_at", withValue: "this")
            lp.addControlParam("nav_to", withValue: "over here")
            lp.addControlParam("random", withValue: UUID.init().uuidString)

            let buo = BranchUniversalObject.init(canonicalIdentifier: "shareResults")
            buo.title = "Simple 3D Tic Tac Toe"
            buo.contentDescription = "Results"
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["status"] = "win"

            buo.showShareSheet(with: lp, andShareText: "Simple 3D Tic Tac Toe Winner!", from: nil, completion: nil)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setConstraints() {
        headerNode.constraints = [
            SKConstraint.positionX(SKRange(constantValue: size.width/2.0)),
            SKConstraint.positionY(SKRange(constantValue: size.height - 80))
        ]
        footerNode.constraints = [
            SKConstraint.positionX(SKRange(constantValue: size.width - 40 )),
            SKConstraint.positionY(SKRange(constantValue: 40))
        ]
        menuButton.constraints = [
            SKConstraint.positionX(SKRange(constantValue: 40)),
            SKConstraint.positionY(SKRange(constantValue: 40))
        ]
        shareResultsButton.constraints = [
            SKConstraint.positionX(SKRange(constantValue: size.width/2.0)),
            SKConstraint.positionY(SKRange(constantValue: size.height - 120))
        ]
    }

    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        setConstraints()
    }
}
