//
//  OverlayScene.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/24/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import SpriteKit

class ButtonNode: SKNode {
    var buttonTapped: (() -> ())?
    var label: SKLabelNode?

    init(text: String?) {
        super.init()
        setup(text: text)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup(text: String?) {
        isUserInteractionEnabled = true

        let labelNode = SKLabelNode(text: text)
        label = labelNode
        addChild(labelNode)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        buttonTapped?()
    }
}
