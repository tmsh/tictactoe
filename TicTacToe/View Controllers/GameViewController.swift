//
//  GameViewController.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/21/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit

class GameViewController: UIViewController {
    var tapGestureRecognizer: UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let sceneView = self.view as? SCNView {
            sceneView.allowsCameraControl = true
            sceneView.autoenablesDefaultLighting = true
            sceneView.isPlaying = true
            // Reduce CPU usage
            sceneView.preferredFramesPerSecond = 30


            tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            sceneView.addGestureRecognizer(tapGestureRecognizer)

            let overlayScene = OverlayScene(size: sceneView.bounds.size)
            sceneView.overlaySKScene = overlayScene

            gameViewModel.overlayScene = overlayScene
            gameViewModel.sceneView = sceneView
            gameViewModel.overlayScene?.restartAction = {
                self.performSegue(withIdentifier: "Restart", sender: nil)
            }
            gameViewModel.allowInputAction = { allow in
                self.tapGestureRecognizer?.isEnabled = allow
            }
        }
    }

    @objc func handleTap(_ gestureRecognizer: UIGestureRecognizer) {
        gameViewModel.handleTap(gestureRecognizer)
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
}
