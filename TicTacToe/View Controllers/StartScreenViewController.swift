//
//  StartScreenViewController.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/21/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController {
    @IBOutlet weak var playerCountSegmentedControl: UISegmentedControl!
    @IBOutlet weak var aiStrengthSegmentedControl: UISegmentedControl!
    @IBOutlet weak var aiStrengthSegmentedControlBackgroundView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var playerOneTextField: UITextField!
    @IBOutlet weak var playerOneLabel: UILabel!
    @IBOutlet weak var playerTwoTextField: UITextField!
    @IBOutlet weak var playerTwoLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        gameViewModel.resetGame(gameType: .humanVersusAI, playerOneName: nil, playerTwoName: nil, aiStrength: aiStrength)

        playerCountSegmentedControl.addTarget(self, action: #selector(segmentedControlChanged), for: .valueChanged)
        startButton.addTarget(self, action: #selector(startButtonTapped(_:)), for: .touchUpInside)
        playerOneTextField.delegate = self
        playerTwoTextField.delegate = self

    }

    private var aiStrength: AIStrength {
        switch aiStrengthSegmentedControl.selectedSegmentIndex {
        case 0:
            return .easy
        case 1:
            return .medium
        case 2:
            return .hard
        default:
            return .hard
        }
    }

    @objc private func segmentedControlChanged(_ control: UISegmentedControl) {
        switch control.selectedSegmentIndex {
        case 0:
            playerTwoLabel.isHidden = true
            playerTwoTextField.isHidden = true
            aiStrengthSegmentedControl.isHidden = false
            aiStrengthSegmentedControlBackgroundView.isHidden = false
        case 1:
            playerTwoLabel.isHidden = false
            playerTwoTextField.isHidden = false
            aiStrengthSegmentedControl.isHidden = true
            aiStrengthSegmentedControlBackgroundView.isHidden = true
        default:
            break
        }
    }

    @objc private func startButtonTapped(_ button: UIButton) {
        isEditing = false
        let playerOneName = playerOneTextField.text == "" ? nil : playerOneTextField.text
        let playerTwoName = playerTwoTextField.text == "" ? nil : playerTwoTextField.text

        switch playerCountSegmentedControl.selectedSegmentIndex {
        case 0:
            gameViewModel.resetGame(gameType: .humanVersusAI, playerOneName: playerOneName, playerTwoName: nil, aiStrength: aiStrength)
            if let playerOneName = playerOneName {
                Branch.getInstance()?.setIdentity(playerOneName + UUID().uuidString)
            } else {
                Branch.getInstance()?.logout()
            }
        case 1:
            gameViewModel.resetGame(gameType: .humanVersusHuman, playerOneName: playerOneName, playerTwoName: playerTwoName, aiStrength: aiStrength)
        default:
            break
        }
        let buo = BranchUniversalObject.init(canonicalIdentifier: "startButtonTapped")
        buo.title = "Start Button Tapped"
        buo.contentMetadata.customMetadata["aiStrength"] = aiStrength.rawValue
        buo.contentMetadata.customMetadata["playerCount"] = playerCountSegmentedControl.selectedSegmentIndex == 0 ? "humanVersusAI" : "humanVersusHuman"
        BranchEvent.standardEvent(.viewItem, withContentItem: buo).logEvent()

        self.performSegue(withIdentifier: "StartGame", sender: nil)
    }
}

extension StartScreenViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        // Begin right away for first person game
        if playerCountSegmentedControl.selectedSegmentIndex == 0 {
            startButtonTapped(startButton)
        }
        return false
    }
}
