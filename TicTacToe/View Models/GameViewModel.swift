//
//  GameViewModel.swift
//  TicTacToe
//
//  Created by Harada, Thomas on 11/22/17.
//  Copyright © 2017 Harada, Thomas. All rights reserved.
//

import SceneKit

// The shared game view model
// We could pass this to different view controllers but for now it's global state.
var gameViewModel = GameViewModel()

class GameViewModel {
    // The main game model
    var gameModel = GameModel(gameType: .humanVersusAI, playerOneName: nil, playerTwoName: nil, aiStrength: .easy)
    // The main scene
    var scnScene: SCNScene!
    // The main camera node
    var cameraNode: SCNNode!
    // The main grid node
    var gridNode = NodeFactory.createGrid()
    // For removing added shape nodes once done with them.
    var addedShapeNodes = [SCNNode]()

    // For setting up the main scene
    var sceneView: SCNView? {
        didSet {
            scnScene = SCNScene()
            scnScene.background.contents = UIImage(named: "Background")
            // from freepbr.com (and https://github.com/asavihay/PBROrbs-iOS10-SceneKit)
            let env = UIImage(named: "spherical.jpg")
            scnScene.lightingEnvironment.contents = env
            scnScene.lightingEnvironment.intensity = 5.0
            sceneView?.scene = scnScene
            sceneView?.backgroundColor = .clear
            scnScene.rootNode.addChildNode(NodeFactory.createCamera(screenSize: UIScreen.main.bounds.size))
            scnScene.rootNode.addChildNode(gridNode)
            resetSameGame()
        }
    }

    // For the UI/HUD
    var overlayScene: OverlayScene?

    // For going back to the start view controller
    var resetAction: (() -> Void)?

    // For disabling tapping between turns
    var allowInputAction: ((Bool) -> Void)?

    /// Reset the game state
    ///
    /// - Parameters:
    ///   - gameType: one or two player
    ///   - playerOneName: optional player one name
    ///   - playerTwoName: optional player two name
    func resetGame(gameType: GameType, playerOneName: String?, playerTwoName: String?, aiStrength: AIStrength?) {
        gameModel = GameModel(gameType: gameType, playerOneName: playerOneName, playerTwoName: playerTwoName, aiStrength: aiStrength)
        overlayScene?.footerText = gameModel.currentTurnText
        self.addedShapeNodes.forEach { $0.removeFromParentNode() }
    }

    /// Convenience method for resetting the same game type
    func resetSameGame() {
        resetGame(gameType: gameModel.gameType, playerOneName: gameModel.playerModels[0].playerName, playerTwoName: gameModel.playerModels[1].playerName, aiStrength: gameModel.aiStrength)
    }

    /// For handling the user input (tap action)
    ///
    /// - Parameter gestureRecognize: a gesture recognizer
    func handleTap(_ gestureRecognizer: UIGestureRecognizer) {
        guard let sceneView = sceneView else {
            return
        }
        let point = gestureRecognizer.location(in: sceneView)
        let hitResults = sceneView.hitTest(point, options: [:])
        if hitResults.count > 0 {
            let nodeName = hitResults[0].node.name
            if let indexes = nodeName?.split(separator: ",").map({ Int($0) }), let leftIndex = indexes[0], let rightIndex = indexes[1] {
                let proposedPosition = GridPosition(row: leftIndex, col: rightIndex)
                guard gameModel.canPlay(position: proposedPosition) else {
                    return
                }
                play(at: proposedPosition)
            }
        }
    }

    /// Add a shape to the game model and grid node
    ///
    /// - Parameters:
    ///   - shape: shape type
    ///   - position: grid position
    private func addShape(shape: PlayerShape, to position: GridPosition) {
        let shapeNode = NodeFactory.createShape(shape: shape, position: position, color: shape == .O ?  UIColor.red : UIColor.green)
        addedShapeNodes.append(shapeNode)
        gridNode.addChildNode(shapeNode)
    }

    /// Handle the game end state if reached
    ///
    /// - Returns: whether a game end state was reached
    private func handleEndState() -> Bool {
        self.overlayScene?.shareResultsIsHidden = true
        if gameModel.isDraw {
            self.overlayScene?.headerText = "It's a draw!"
            gridNode.spinBoard()

            allowInputAction?(false)
            let deadlineTime = DispatchTime.now() + .seconds(2)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.allowInputAction?(true)
                self.overlayScene?.headerText = ""
                self.resetSameGame()
            }
            return true
        } else if gameModel.hasWinner {
            self.allowInputAction?(false)
            self.overlayScene?.headerText = self.gameModel.winnerText
            self.overlayScene?.shareResultsIsHidden = false
            // Use opposite, since turn has rotated.
            emitConfettiShapes(shape: gameModel.activePlayerShape)

            let deadlineTime = DispatchTime.now() + .seconds(5)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.allowInputAction?(true)
                self.overlayScene?.headerText = ""
                self.overlayScene?.shareResultsIsHidden = true
                self.resetSameGame()
            }
            return true
        }
        return false
    }

    /// Rotate the player in the game state and update the footer text
    private func rotatePlayer() {
        gameModel.rotatePlayer()
        overlayScene?.footerText = gameModel.currentTurnText
    }

    /// Play the active player's turn at a grid position
    ///
    /// - Parameter gridPosition: a grid position
    func play(at gridPosition: GridPosition) {
        addShape(shape: gameModel.activePlayerShape, to: gridPosition)
        gameModel.updateActivePlayer(position: gridPosition)
        if handleEndState() {
            return
        }
        rotatePlayer()
        if gameModel.gameType == .humanVersusAI, let aiPosition = gameModel.getBestPositionForActivePlayer() {
            allowInputAction?(false)
            let deadlineTime = DispatchTime.now() + .microseconds(500000)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.allowInputAction?(true)
                self.addShape(shape: self.gameModel.activePlayerShape, to: aiPosition)
                self.gameModel.updateActivePlayer(position: aiPosition)
                if self.handleEndState() {
                    return
                }
                self.rotatePlayer()
            }
        }
        // always check for draw at the bottom
        if handleEndState() {
            return
        }
    }
}

extension GameViewModel { // Winning animation
    /// Emit shapes (for the winning animation)
    ///
    /// - Parameter shape: the shape to emit
    private func emitConfettiShapes(shape: PlayerShape) {
        for _ in 0...19 {
            emitConfettiShape(shape: shape)
        }
    }

    /// Emit a single shape
    ///
    /// - Parameter shape: the shape to emit
    private func emitConfettiShape(shape: PlayerShape) {
        let shapeNode = NodeFactory.createShape(shape: shape, position: GridPosition(row: 0, col: 0), color: UIColor.random())
        let scaleValue = Float.random(min: 0.25, max: 0.5)
        shapeNode.scale = SCNVector3(x: scaleValue, y: scaleValue, z: scaleValue)

        addedShapeNodes.append(shapeNode)
        scnScene.rootNode.addChildNode(shapeNode)

        shapeNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)

        let randomX = Float.random(min: -3, max: 3)
        let randomY = Float.random(min: 8, max: 20)

        let force = SCNVector3(x: randomX, y: randomY, z: 0)
        let position = SCNVector3(x: 0.07, y: 0.07, z: 0.07)

        shapeNode.physicsBody?.applyForce(force, at: position, asImpulse: true)
    }
}
